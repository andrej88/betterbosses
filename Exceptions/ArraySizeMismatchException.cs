﻿using System;
using Terraria.ModLoader;

namespace BetterBosses.Exceptions
{
    class ArraySizeMismatchException : Exception
    {
        public ArraySizeMismatchException()
        {
            ErrorLogger.Log("Array Size Mismatch Exception!");
        }
    }
}
