﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BetterBosses.Exceptions
{
    class InvalidTrialNumberException : Exception
    {
        public InvalidTrialNumberException(string message) : base(message)
        {
        }

        public InvalidTrialNumberException() : base()
        {
        }
    }
}
