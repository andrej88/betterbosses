﻿using BetterBosses.Exceptions;
using System;
using System.Collections.Generic;
using Terraria.ID;


namespace BetterBosses.NPCs
{
    class LootTable
    {
        private List<ItemDropSet> loots;

        public LootTable()
        {
            loots = new List<ItemDropSet>();
        }

        public LootTable(List<ItemDropSet> loots)
        {
            this.loots = loots;
        }

        public void AddSet(ItemDropSet itemDropSet)
        {
            loots.Add(itemDropSet);
        }

        public void AddSet(ICollection<ItemDropChance> items, int trials = 1)
        {
            AddSet(new ItemDropSet(items, trials));
        }

        public void AddSet(int[] drops, int[] weights, int trials = 1)
        {
            HashSet<ItemDropChance> items = new HashSet<ItemDropChance>();

            foreach (int i in drops)
            {
                ItemDropChance c;
                try
                {
                    c = new ItemDropChance(drops[i], weights[i], 1, 1);
                }
                catch (IndexOutOfRangeException e)
                {
                    throw new ArraySizeMismatchException();
                }

                items.Add(c);
            }

            AddSet(items, trials);
            
        }

        public List<ItemStack> GetRandomLootDrop()
        {
            List<ItemStack> drops = new List<ItemStack>();

            foreach (ItemDropSet i in loots)
            {
                drops.AddRange(i.ItemsToDrop());
            }

            return drops;
        }
    }
}
