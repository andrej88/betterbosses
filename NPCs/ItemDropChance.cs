﻿using System;

namespace BetterBosses.NPCs
{
    class ItemDropChance
    {
        private int id;
        private int weight;
        private int min;
        private int max;
        private ItemDropChance companion;

        public ItemDropChance(int id, int weight, int min, int max, ItemDropChance companion = null)
        {
            this.id = id;
            if (weight < 1)
                throw new ArgumentException("weight must be greater than 0", "weight");
            this.weight = weight;
            if (max < min)
                throw new ArgumentException("max may not be less than min");
            this.max = max;
            this.min = min;
        }

        public ItemDropChance(int id, int weight, int num, ItemDropChance companion = null)
        {
            this.id = id;
            if (weight < 1)
                throw new ArgumentException("weight must be greater than 0", "weight");
            this.weight = weight;
            if (max < min)
                throw new ArgumentException("max may not be less than min");
            this.max = num;
            this.min = num;
        }

        public ItemDropChance(int id, int weight = 1, ItemDropChance companion = null)
        {
            this.id = id;
            if (weight < 1)
                throw new ArgumentException("weight must be greater than 0", "weight");
            this.weight = weight;
            if (max < min)
                throw new ArgumentException("max may not be less than min");
            this.max = 1;
            this.min = 1;
        }

        //public ItemDropChance()
        //{
        //    this.id = 0;
        //    this.weight = 1;
        //    this.max = 2;
        //    this.min = 1;
        //}

        public int GetID()
        {
            return id;
        }

        public int GetWeight()
        {
            return weight;
        }

        public int GetMin()
        {
            return min;
        }

        public int GetMax()
        {
            return max;
        }

        public ItemDropChance GetCompanion()
        {
            return companion;
        }

        public bool Equals(ItemDropChance i)
        {
            if (i == null)
                return false;
            if (companion == null)
                return (id == i.GetID() && weight == i.GetWeight() && max == i.GetMax() && min == i.GetMin());
            else
                return (id == i.GetID() && weight == i.GetWeight() && max == i.GetMax() && min == i.GetMin() && companion == i.GetCompanion());
        }
    }

}
