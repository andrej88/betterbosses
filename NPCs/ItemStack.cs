﻿
namespace BetterBosses.NPCs
{
    struct ItemStack
    {
        public int id;
        public int stack;

        public ItemStack(int id, int stack)
        {
            this.id = id;
            this.stack = stack;
        }

        public bool Equals(ItemStack i)
        {
            return (id == i.id && stack == i.stack);
        }
    }
}
