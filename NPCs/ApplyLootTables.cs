﻿using System.Collections;
using System.Collections.Generic;
using Terraria;
using Terraria.GameContent.Achievements;
using Terraria.ID;
using Terraria.ModLoader;

namespace BetterBosses.NPCs
{
    class npcnpcApplyLootTables : GlobalNPC
    {
        private List<int> bossIDs = new List<int>(new int[]{
                NPCID.KingSlime,
                NPCID.EyeofCthulhu,
                NPCID.EaterofWorldsHead,
                NPCID.EaterofWorldsBody,
                NPCID.EaterofWorldsTail,
                NPCID.SkeletronHead,
                NPCID.QueenBee,
                NPCID.WallofFlesh,
                NPCID.Retinazer,
                NPCID.Spazmatism,
                NPCID.TheDestroyer,
                NPCID.SkeletronPrime,
                NPCID.Plantera,
                NPCID.Golem,
                NPCID.DukeFishron,
                //NPCID.MartianSaucer,
                //NPCID.MourningWood,
                //NPCID.Pumpking,
                //NPCID.Everscream,
                //NPCID.SantaNK1,
                //NPCID.IceQueen,
                NPCID.MoonLordCore
            });

        public override bool PreNPCLoot(NPC npc)
        {
            
            if (bossIDs.Contains(npc.type))
            {


                bool flag = NPC.downedMechBoss1 && NPC.downedMechBoss2 && NPC.downedMechBoss3;

                // Drop Hearts
                if (npc.boss)
                {
                    //int numHearts = Main.rand.Next(5) + 5;
                    //for (int i = 0; i < numHearts; i++)
                    //{
                    //    Item.NewItem((int)npc.position.X, (int)npc.position.Y, npc.width, npc.height, ItemID.Heart);
                    //}

                    if (npc.type == 4)
                    {
                        NPC.downedBoss1 = true;
                    }
                    else if (npc.type == 13 || npc.type == 14 || npc.type == 15)
                    {
                        NPC.downedBoss2 = true;
                        npc.name = "Eater of Worlds";
                    }
                    else if (npc.type == 266)
                    {
                        NPC.downedBoss2 = true;
                        npc.name = "Brain of Cthulu";
                    }
                    else if (npc.type == 35)
                    {
                        NPC.downedBoss3 = true;
                        npc.name = "Skeletron";
                    }
                    else
                    {
                        npc.name = npc.displayName;
                    }
                    if (npc.type == 127)
                    {
                        NPC.downedMechBoss3 = true;
                        NPC.downedMechBossAny = true;
                    }
                    if (npc.type == 134)
                    {
                        NPC.downedMechBoss1 = true;
                        NPC.downedMechBossAny = true;
                    }
                    string name = npc.name;
                    if (npc.displayName != "")
                    {
                        name = npc.displayName;
                    }
                    int num71 = Main.rand.Next(5) + 5;
                    for (int num72 = 0; num72 < num71; num72++)
                    {
                        Item.NewItem((int)npc.position.X, (int)npc.position.Y, npc.width, npc.height, 58, 1, false, 0, false, false);
                    }
                    if (npc.type == 125 || npc.type == 126)
                    {
                        NPC.downedMechBoss2 = true;
                        NPC.downedMechBossAny = true;
                        if (Main.netMode == 0)
                        {
                            Main.NewText("The Twins " + Lang.misc[50], 175, 75, 255, false);
                        }
                        else if (Main.netMode == 2)
                        {
                            NetMessage.SendData(25, -1, -1, "The Twins " + Lang.misc[50], 255, 175f, 75f, 255f, 0, 0, 0);
                        }
                    }
                    else if (npc.type == 398)
                    {
                        if (Main.netMode == 0)
                        {
                            Main.NewText("Moon Lord " + Lang.misc[17], 175, 75, 255, false);
                        }
                        else if (Main.netMode == 2)
                        {
                            NetMessage.SendData(25, -1, -1, "Moon Lord " + Lang.misc[17], 255, 175f, 75f, 255f, 0, 0, 0);
                        }
                    }
                    else if (Main.netMode == 0)
                    {
                        Main.NewText(name + " " + Lang.misc[17], 175, 75, 255, false);
                    }
                    else if (Main.netMode == 2)
                    {
                        NetMessage.SendData(25, -1, -1, name + " " + Lang.misc[17], 255, 175f, 75f, 255f, 0, 0, 0);
                    }
                    if (npc.type == 113 && Main.netMode != 1)
                    {
                        bool hardMode = Main.hardMode;
                        WorldGen.StartHardmode();
                        if (NPC.downedMechBoss1 && NPC.downedMechBoss2 && NPC.downedMechBoss3 && !hardMode)
                        {
                            if (Main.netMode == 0)
                            {
                                Main.NewText(Lang.misc[32], 50, 255, 130, false);
                            }
                            else if (Main.netMode == 2)
                            {
                                NetMessage.SendData(25, -1, -1, Lang.misc[32], 255, 50f, 255f, 130f, 0, 0, 0);
                            }
                        }
                    }
                    if (Main.netMode == 2)
                    {
                        NetMessage.SendData(7, -1, -1, "", 0, 0f, 0f, 0f, 0, 0, 0);
                    }
                }

                if (npc.type == NPCID.KingSlime)
                {
                    if (Main.slimeRain)
                    {
                        Main.StopSlimeRain(true);
                        AchievementsHelper.NotifyProgressionEvent(16);
                    }
                    if (Main.expertMode)
                        npc.DropBossBags();
                    else
                    {
                        List<ItemStack> drops = VanillaLootTables.KingSlimeNormal.GetRandomLootDrop();
                        foreach (ItemStack i in drops)
                        {
                            Item.NewItem((int)npc.position.X, (int)npc.position.Y, npc.width, npc.height, i.id, i.stack);
                        }
                    }
                    NPC.downedSlimeKing = true;
                    if (Main.netMode == 2)
                    {
                        NetMessage.SendData(7, -1, -1, "", 0, 0f, 0f, 0f, 0, 0, 0);
                    }
                }

                else if (npc.type == NPCID.EyeofCthulhu)
                {
                    if (Main.expertMode)
                        npc.DropBossBags();
                    else
                    {
                        if (WorldGen.crimson)
                        {
                            List<ItemStack> drops = VanillaLootTables.EyeOfCthulhuNormalCrimson.GetRandomLootDrop();
                            foreach (ItemStack i in drops)
                            {
                                Item.NewItem((int)npc.position.X, (int)npc.position.Y, npc.width, npc.height, i.id, i.stack);
                            }
                        }
                        else
                        {
                            {
                                List<ItemStack> drops = VanillaLootTables.EyeOfCthulhuNormalCorrupt.GetRandomLootDrop();
                                foreach (ItemStack i in drops)
                                {
                                    Item.NewItem((int)npc.position.X, (int)npc.position.Y, npc.width, npc.height, i.id, i.stack);
                                }
                            }
                        }
                    }
                }

                else if (npc.type == NPCID.EaterofWorldsHead || npc.type == NPCID.EaterofWorldsBody || npc.type == NPCID.EaterofWorldsTail)
                {

                    if (Main.rand.Next(2) == 0)
                    {
                        if (Main.expertMode)
                            Item.NewItem((int)npc.position.X, (int)npc.position.Y, npc.width, npc.height, ItemID.ShadowScale, Main.rand.Next(2, 4));
                        else
                            Item.NewItem((int)npc.position.X, (int)npc.position.Y, npc.width, npc.height, ItemID.ShadowScale, Main.rand.Next(1, 3));
                    }
                    if (Main.rand.Next(2) == 0)
                    {
                        Item.NewItem((int)npc.position.X, (int)npc.position.Y, npc.width, npc.height, ItemID.DemoniteOre, Main.rand.Next(2, 6));
                    }

                    if (npc.boss)
                    {
                        if (Main.expertMode)
                            npc.DropBossBags();
                        else
                        {
                            List<ItemStack> drops = VanillaLootTables.EaterOfWorldsNormal.GetRandomLootDrop();
                            foreach (ItemStack i in drops)
                            {
                                Item.NewItem((int)npc.position.X, (int)npc.position.Y, npc.width, npc.height, i.id, i.stack);
                            }
                        }
                    }

                    if (Main.rand.Next(4) == 0 && Main.player[(int)Player.FindClosest(npc.position, npc.width, npc.height)].statLife < Main.player[(int)Player.FindClosest(npc.position, npc.width, npc.height)].statLifeMax2)
                    {
                        Item.NewItem((int)npc.position.X, (int)npc.position.Y, npc.width, npc.height, 58);
                    }
                }

                else if (npc.type == NPCID.BrainofCthulhu)
                {
                    if (Main.expertMode)
                        npc.DropBossBags();
                    else
                    {
                        List<ItemStack> drops = VanillaLootTables.BrainOfCthulhuNormal.GetRandomLootDrop();
                        foreach (ItemStack i in drops)
                        {
                            Item.NewItem((int)npc.position.X, (int)npc.position.Y, npc.width, npc.height, i.id, i.stack);
                        }
                    }
                }

                else if (npc.type == NPCID.SkeletronHead)
                {
                    if (Main.expertMode)
                        npc.DropBossBags();
                    else
                    {
                        List<ItemStack> drops = VanillaLootTables.SkeletronNormal.GetRandomLootDrop();
                        foreach (ItemStack i in drops)
                        {
                            Item.NewItem((int)npc.position.X, (int)npc.position.Y, npc.width, npc.height, i.id, i.stack);
                        }
                    }
                }

                else if (npc.type == NPCID.QueenBee)
                {
                    if (Main.expertMode)
                        npc.DropBossBags();
                    else
                    {
                        List<ItemStack> drops = VanillaLootTables.QueenBeeNormal.GetRandomLootDrop();
                        foreach (ItemStack i in drops)
                        {
                            Item.NewItem((int)npc.position.X, (int)npc.position.Y, npc.width, npc.height, i.id, i.stack);
                        }
                    }
                    NPC.downedQueenBee = true;
                    if (Main.netMode == 2)
                    {
                        NetMessage.SendData(7, -1, -1, "", 0, 0f, 0f, 0f, 0, 0, 0);
                    }
                }

                else if (npc.type == NPCID.WallofFlesh)
                {
                    if (Main.expertMode)
                        npc.DropBossBags();
                    else
                    {
                        List<ItemStack> drops = VanillaLootTables.WallOfFleshNormal.GetRandomLootDrop();
                        foreach (ItemStack i in drops)
                        {
                            Item.NewItem((int)npc.position.X, (int)npc.position.Y, npc.width, npc.height, i.id, i.stack);
                        }
                    }

                    if (Main.netMode != 1)
                    {
                        int num51 = (int)(npc.position.X + (float)(npc.width / 2)) / 16;
                        int num52 = (int)(npc.position.Y + (float)(npc.height / 2)) / 16;
                        int num53 = npc.width / 2 / 16 + 1;
                        for (int num54 = num51 - num53; num54 <= num51 + num53; num54++)
                        {
                            for (int num55 = num52 - num53; num55 <= num52 + num53; num55++)
                            {
                                if ((num54 == num51 - num53 || num54 == num51 + num53 || num55 == num52 - num53 || num55 == num52 + num53) && !Main.tile[num54, num55].active())
                                {
                                    Main.tile[num54, num55].type = (ushort)(WorldGen.crimson ? 347 : 140);
                                    Main.tile[num54, num55].active(true);
                                }
                                Main.tile[num54, num55].lava(false);
                                Main.tile[num54, num55].liquid = 0;
                                if (Main.netMode == 2)
                                {
                                    NetMessage.SendTileSquare(-1, num54, num55, 1);
                                }
                                else
                                {
                                    WorldGen.SquareTileFrame(num54, num55, true);
                                }
                            }
                        }
                    }
                }

                else if (npc.type == NPCID.Spazmatism || npc.type == NPCID.Retinazer)
                {
                    if (Main.rand.Next(10) == 0)
                    {
                        if (npc.type == NPCID.Spazmatism)
                            Item.NewItem((int)npc.position.X, (int)npc.position.Y, npc.width, npc.height, ItemID.SpazmatismTrophy);
                        else
                            Item.NewItem((int)npc.position.X, (int)npc.position.Y, npc.width, npc.height, ItemID.RetinazerTrophy);
                    }

                    int otherTwin = NPCID.Retinazer;
                    if (npc.type == NPCID.Retinazer)
                    {
                        otherTwin = NPCID.Spazmatism;
                    }

                    if (!NPC.AnyNPCs(otherTwin))
                    {
                        if (Main.expertMode)
                            npc.DropBossBags();
                        else
                        {
                            List<ItemStack> drops = VanillaLootTables.TheTwinsNormal.GetRandomLootDrop();
                            foreach (ItemStack i in drops)
                            {
                                Item.NewItem((int)npc.position.X, (int)npc.position.Y, npc.width, npc.height, i.id, i.stack);
                            }
                        }
                    }
                    else
                    {
                        npc.value = 0f;
                        npc.boss = false;
                    }

                }

                else if (npc.type == NPCID.SkeletronPrime)
                {
                    if (Main.expertMode)
                        npc.DropBossBags();
                    else
                    {
                        List<ItemStack> drops = VanillaLootTables.SkeletronPrimeNormal.GetRandomLootDrop();
                        foreach (ItemStack i in drops)
                        {
                            Item.NewItem((int)npc.position.X, (int)npc.position.Y, npc.width, npc.height, i.id, i.stack);
                        }
                    }
                }

                else if (npc.type == NPCID.TheDestroyer)
                {
                    if (Main.expertMode)
                        npc.DropBossBags();
                    else
                    {
                        List<ItemStack> drops = VanillaLootTables.TheDestroyerNormal.GetRandomLootDrop();
                        foreach (ItemStack i in drops)
                        {
                            Item.NewItem((int)npc.position.X, (int)npc.position.Y, npc.width, npc.height, i.id, i.stack);
                        }
                    }
                }

                else if (npc.type == NPCID.Plantera)
                {
                    if (Main.expertMode)
                        npc.DropBossBags();
                    else
                    {
                        List<ItemStack> drops;
                        if (!NPC.downedPlantBoss)
                        {
                            drops = VanillaLootTables.PlanteraNormalFirstKill.GetRandomLootDrop();
                        }
                        else
                        {
                            drops = VanillaLootTables.PlanteraNormal.GetRandomLootDrop();
                        }
                        foreach (ItemStack i in drops)
                        {
                            Item.NewItem((int)npc.position.X, (int)npc.position.Y, npc.width, npc.height, i.id, i.stack);
                        }
                    }
                    bool flag3 = NPC.downedPlantBoss;
                    NPC.downedPlantBoss = true;
                    if (!flag3)
                    {
                        if (Main.netMode == 0)
                        {
                            Main.NewText(Lang.misc[33], 50, 255, 130, false);
                        }
                        else if (Main.netMode == 2)
                        {
                            NetMessage.SendData(25, -1, -1, Lang.misc[33], 255, 50f, 255f, 130f, 0, 0, 0);
                        }
                    }
                }

                else if (npc.type == NPCID.Golem)
                {
                    if (Main.expertMode)
                        npc.DropBossBags();
                    else
                    {
                        List<ItemStack> drops = VanillaLootTables.GolemNormal.GetRandomLootDrop();
                        foreach (ItemStack i in drops)
                        {
                            Item.NewItem((int)npc.position.X, (int)npc.position.Y, npc.width, npc.height, i.id, i.stack);
                        }
                    }
                    NPC.downedGolemBoss = true;
                }

                else if (npc.type == NPCID.DukeFishron)
                {
                    if (Main.expertMode)
                        npc.DropBossBags();
                    else
                    {
                        List<ItemStack> drops = VanillaLootTables.DukeFishronNormal.GetRandomLootDrop();
                        foreach (ItemStack i in drops)
                        {
                            Item.NewItem((int)npc.position.X, (int)npc.position.Y, npc.width, npc.height, i.id, i.stack);
                        }
                    }
                    NPC.downedFishron = true;
                }

                else if (npc.type == NPCID.MoonLordCore)
                {
                    NPC.downedMoonlord = true;
                    NPC.LunarApocalypseIsUp = false;
                    if (Main.expertMode)
                        npc.DropBossBags();
                    else
                    {
                        List<ItemStack> drops = VanillaLootTables.QueenBeeNormal.GetRandomLootDrop();
                        foreach (ItemStack i in drops)
                        {
                            Item.NewItem((int)npc.position.X, (int)npc.position.Y, npc.width, npc.height, i.id, i.stack);
                        }
                    }
                }

                if (!flag && NPC.downedMechBoss1 && NPC.downedMechBoss2 && NPC.downedMechBoss3 && Main.hardMode)
                {
                    if (Main.netMode == 0)
                    {
                        Main.NewText(Lang.misc[32], 50, 255, 130, false);
                    }
                    else if (Main.netMode == 2)
                    {
                        NetMessage.SendData(25, -1, -1, Lang.misc[32], 255, 50f, 255f, 130f, 0, 0, 0);
                    }
                }

                //TODO: Add boss bags for bosses without boss bags... maybe except for bosses with wave dependent drops. They could drop an amount of essence dependent on the wave + a boss bag with non-wave dependent items? Or have a wave-dependent chance of dropping a boss bag
                //TODO: Add boss "essence"

                return false;
            }

            return true;
        }

        public override void NPCLoot(NPC npc)
        {

            

        }

    }
}
