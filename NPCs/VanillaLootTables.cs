﻿using System.Collections.Generic;
using Terraria.ID;

namespace BetterBosses.NPCs
{
    class VanillaLootTables
    {

        // Pre Hard Mode
        #region PreHardMode
        public static readonly LootTable KingSlimeNormal = new LootTable(new List<ItemDropSet> {
                        new ItemDropSet(new HashSet<ItemDropChance>() { 
                            new ItemDropChance(ItemID.SlimeHook, 1),
                            new ItemDropChance(ItemID.SlimeGun, 1),
                            new ItemDropChance(0, 1)
                        }),
                        new ItemDropSet(new HashSet<ItemDropChance>() {
                            new ItemDropChance(ItemID.SlimySaddle, 1),
                            new ItemDropChance(0, 3)
                        }),
                        new ItemDropSet(new HashSet<ItemDropChance>() {
                            new ItemDropChance(ItemID.Solidifier, 1),
                            new ItemDropChance(0, 2)
                        }),
                        new ItemDropSet(new HashSet<ItemDropChance>() {
                            new ItemDropChance(ItemID.NinjaHood, 1),
                            new ItemDropChance(ItemID.NinjaShirt, 1),
                            new ItemDropChance(ItemID.NinjaPants, 1)
                        }),
                        new ItemDropSet(new HashSet<ItemDropChance>() {
                            new ItemDropChance(ItemID.KingSlimeMask, 1),
                            new ItemDropChance(0, 6)
                        }),
                        new ItemDropSet(new HashSet<ItemDropChance>() {
                            new ItemDropChance(ItemID.KingSlimeTrophy, 1),
                            new ItemDropChance(0, 9)
                        }),
                        new ItemDropSet(new HashSet<ItemDropChance>() {
                            new ItemDropChance(ItemID.LesserHealingPotion, 1, 5, 15)
                        }),
                        new ItemDropSet(new HashSet<ItemDropChance>() {
                            new ItemDropChance(ItemID.GoldCoin, 1)
                        })
                    });
        public static readonly LootTable KingSlimeExpert = new LootTable(new List<ItemDropSet> {
                        new ItemDropSet(new HashSet<ItemDropChance>() { 
                            new ItemDropChance(ItemID.SlimeHook, 1),
                            new ItemDropChance(ItemID.SlimeGun, 1)
                        }),
                        new ItemDropSet(new HashSet<ItemDropChance>() {
                            new ItemDropChance(ItemID.SlimySaddle, 1),
                            new ItemDropChance(0, 1)
                        }),
                        new ItemDropSet(new HashSet<ItemDropChance>() {
                            new ItemDropChance(ItemID.Solidifier, 1)
                        }),
                        new ItemDropSet(new HashSet<ItemDropChance>() {
                            new ItemDropChance(ItemID.NinjaHood, 1),
                            new ItemDropChance(ItemID.NinjaShirt, 1),
                            new ItemDropChance(ItemID.NinjaPants, 1)
                        }, 2),
                        new ItemDropSet(new HashSet<ItemDropChance>() {
                            new ItemDropChance(ItemID.KingSlimeMask, 1),
                            new ItemDropChance(0, 6)
                        }),
                        new ItemDropSet(new HashSet<ItemDropChance>() {
                            new ItemDropChance(ItemID.KingSlimeTrophy, 1),
                            new ItemDropChance(0, 9)
                        }),
                        new ItemDropSet(new HashSet<ItemDropChance>() {
                            new ItemDropChance(ItemID.LesserHealingPotion, 1, 5, 15)
                        }),
                        new ItemDropSet(new HashSet<ItemDropChance>() {
                            new ItemDropChance(ItemID.GoldCoin, 1)
                        }),
                        new ItemDropSet(new HashSet<ItemDropChance>() {
                            new ItemDropChance(ItemID.RoyalGel, 1)
                        })
                    });

        public static readonly LootTable EyeOfCthulhuNormalCorrupt = new LootTable(new List<ItemDropSet> {
                        new ItemDropSet(new HashSet<ItemDropChance>() { 
                            new ItemDropChance(ItemID.DemoniteOre, 1, 30, 88)
                        }),
                        new ItemDropSet(new HashSet<ItemDropChance>() { 
                            new ItemDropChance(ItemID.UnholyArrow, 1, 20, 50)
                        }),
                        new ItemDropSet(new HashSet<ItemDropChance>() { 
                            new ItemDropChance(ItemID.CorruptSeeds, 1, 1, 4)
                        }),
                        new ItemDropSet(new HashSet<ItemDropChance>() { 
                            new ItemDropChance(ItemID.LesserHealingPotion, 1, 5, 15)
                        }),
                        new ItemDropSet(new HashSet<ItemDropChance>() { 
                            new ItemDropChance(ItemID.EyeMask, 1),
                            new ItemDropChance(0, 6)
                        }),
                        new ItemDropSet(new HashSet<ItemDropChance>() { 
                            new ItemDropChance(ItemID.EyeofCthulhuTrophy, 1),
                            new ItemDropChance(0, 9)
                        }),
                        new ItemDropSet(new HashSet<ItemDropChance>() { 
                            new ItemDropChance(ItemID.Binoculars, 1),
                            new ItemDropChance(0, 49)
                        }),
                        new ItemDropSet(new HashSet<ItemDropChance>() { 
                            new ItemDropChance(ItemID.GoldCoin, 1, 3)
                        })
                    });
        public static readonly LootTable EyeOfCthulhuNormalCrimson = new LootTable(new List<ItemDropSet> {
                        new ItemDropSet(new HashSet<ItemDropChance>() { 
                            new ItemDropChance(ItemID.CrimtaneOre, 1, 30, 88)
                        }),
                        new ItemDropSet(new HashSet<ItemDropChance>() { 
                            new ItemDropChance(ItemID.CrimsonSeeds, 1, 1, 4)
                        }),
                        new ItemDropSet(new HashSet<ItemDropChance>() { 
                            new ItemDropChance(ItemID.LesserHealingPotion, 1, 5, 15)
                        }),
                        new ItemDropSet(new HashSet<ItemDropChance>() { 
                            new ItemDropChance(ItemID.EyeMask, 1),
                            new ItemDropChance(0, 6)
                        }),
                        new ItemDropSet(new HashSet<ItemDropChance>() { 
                            new ItemDropChance(ItemID.EyeofCthulhuTrophy, 1),
                            new ItemDropChance(0, 9)
                        }),
                        new ItemDropSet(new HashSet<ItemDropChance>() { 
                            new ItemDropChance(ItemID.Binoculars, 1),
                            new ItemDropChance(0, 49)
                        }),
                        new ItemDropSet(new HashSet<ItemDropChance>() { 
                            new ItemDropChance(ItemID.GoldCoin, 1, 3)
                        })
                    });
        public static readonly LootTable EyeOfCthulhuExpertCorrupt = new LootTable(new List<ItemDropSet> {
                        new ItemDropSet(new HashSet<ItemDropChance>() { 
                            new ItemDropChance(ItemID.DemoniteOre, 1, 30, 88)
                        }),
                        new ItemDropSet(new HashSet<ItemDropChance>() { 
                            new ItemDropChance(ItemID.UnholyArrow, 1, 20, 50)
                        }),
                        new ItemDropSet(new HashSet<ItemDropChance>() { 
                            new ItemDropChance(ItemID.CorruptSeeds, 1, 1, 4)
                        }),
                        new ItemDropSet(new HashSet<ItemDropChance>() { 
                            new ItemDropChance(ItemID.LesserHealingPotion, 1, 5, 15)
                        }),
                        new ItemDropSet(new HashSet<ItemDropChance>() { 
                            new ItemDropChance(ItemID.EyeMask, 1),
                            new ItemDropChance(0, 6)
                        }),
                        new ItemDropSet(new HashSet<ItemDropChance>() { 
                            new ItemDropChance(ItemID.EyeofCthulhuTrophy, 1),
                            new ItemDropChance(0, 9)
                        }),
                        new ItemDropSet(new HashSet<ItemDropChance>() { 
                            new ItemDropChance(ItemID.Binoculars, 1),
                            new ItemDropChance(0, 29)
                        }),
                        new ItemDropSet(new HashSet<ItemDropChance>() { 
                            new ItemDropChance(ItemID.GoldCoin, 1, 3)
                        }),
                        new ItemDropSet(new HashSet<ItemDropChance>{
                            new ItemDropChance(ItemID.EoCShield)
                        })
                    });
        public static readonly LootTable EyeOfCthulhuExpertCrimson = new LootTable(new List<ItemDropSet> {
                        new ItemDropSet(new HashSet<ItemDropChance>() { 
                            new ItemDropChance(ItemID.CrimtaneOre, 1, 30, 88)
                        }),
                        new ItemDropSet(new HashSet<ItemDropChance>() { 
                            new ItemDropChance(ItemID.CrimsonSeeds, 1, 1, 4)
                        }),
                        new ItemDropSet(new HashSet<ItemDropChance>() { 
                            new ItemDropChance(ItemID.LesserHealingPotion, 1, 5, 15)
                        }),
                        new ItemDropSet(new HashSet<ItemDropChance>() { 
                            new ItemDropChance(ItemID.EyeMask, 1),
                            new ItemDropChance(0, 6)
                        }),
                        new ItemDropSet(new HashSet<ItemDropChance>() { 
                            new ItemDropChance(ItemID.EyeofCthulhuTrophy, 1),
                            new ItemDropChance(0, 9)
                        }),
                        new ItemDropSet(new HashSet<ItemDropChance>() { 
                            new ItemDropChance(ItemID.Binoculars, 1),
                            new ItemDropChance(0, 29)
                        }),
                        new ItemDropSet(new HashSet<ItemDropChance>() { 
                            new ItemDropChance(ItemID.GoldCoin, 1, 3)
                        }),
                        new ItemDropSet(new HashSet<ItemDropChance>{
                            new ItemDropChance(ItemID.EoCShield)
                        })
                    });

        public static readonly LootTable EaterOfWorldsNormal = new LootTable(new List<ItemDropSet>{
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.ShadowScale, 1, 29, 58)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.DemoniteOre, 1, 20, 60)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.LesserHealingPotion, 1, 5, 15)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.EaterMask, 1),
                new ItemDropChance(0, 6)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.EaterofWorldsTrophy, 1),
                new ItemDropChance(0, 9)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.EatersBone, 1),
                new ItemDropChance(0, 19)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.SilverCoin, 3),
            })
        });
        public static readonly LootTable EaterOfWorldsExpert = new LootTable(new List<ItemDropSet>{
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.ShadowScale, 1, 29, 58)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.DemoniteOre, 1, 20, 60)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.LesserHealingPotion, 1, 5, 15)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.EaterMask, 1),
                new ItemDropChance(0, 6)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.EaterofWorldsTrophy, 1),
                new ItemDropChance(0, 9)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.EatersBone, 1),
                new ItemDropChance(0, 19)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.SilverCoin, 1, 3),
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.WormScarf),
            })
        });

        public static readonly LootTable BrainOfCthulhuNormal = new LootTable(new List<ItemDropSet>{
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.BrainMask, 1),
                new ItemDropChance(0, 6)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.BrainofCthulhuTrophy, 1),
                new ItemDropChance(0, 9)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.CrimtaneOre, 1, 40, 91)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.LesserHealingPotion, 1, 5, 15)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.TissueSample, 1, 30, 50)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.BoneRattle, 1),
                new ItemDropChance(0, 19)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.GoldCoin),
            })
        });
        public static readonly LootTable BrainOfCthulhuExpert = new LootTable(new List<ItemDropSet>{
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.BrainMask, 1),
                new ItemDropChance(0, 6)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.BrainofCthulhuTrophy, 1),
                new ItemDropChance(0, 9)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.CrimtaneOre, 1, 40, 91)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.LesserHealingPotion, 1, 5, 15)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.TissueSample, 1, 10, 20)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.BoneRattle, 1),
                new ItemDropChance(0, 19)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.GoldCoin, 1, 1)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.BrainOfConfusion)
            })
        });

        public static readonly LootTable SkeletronNormal = new LootTable(new List<ItemDropSet>{
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.SkeletronMask, 49),
                new ItemDropChance(ItemID.SkeletronHand, 42),
                new ItemDropChance(ItemID.BookofSkulls, 36),
                new ItemDropChance(0, 216)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.SkeletronTrophy, 1),
                new ItemDropChance(0, 9)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.LesserHealingPotion, 1, 5, 15)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.GoldCoin, 1, 5)
            })
        });
        public static readonly LootTable SkeletronExpert = new LootTable(new List<ItemDropSet>{
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.SkeletronMask, 1),
                new ItemDropChance(ItemID.SkeletronHand, 1),
                new ItemDropChance(ItemID.BookofSkulls, 1)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.SkeletronTrophy, 1),
                new ItemDropChance(0, 9)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.LesserHealingPotion, 1, 5, 15)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.GoldCoin, 1, 5)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.BoneGlove, 1)
            })
        });

        public static readonly LootTable QueenBeeNormal = new LootTable(new List<ItemDropSet>{
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.BeeGun, 1),
                new ItemDropChance(ItemID.BeeKeeper, 1),
                new ItemDropChance(ItemID.BeesKnees, 1)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.HoneyComb, 1),
                new ItemDropChance(0, 2)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.Nectar, 1),
                new ItemDropChance(0, 14)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.HoneyedGoggles, 1),
                new ItemDropChance(0, 14)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.HiveWand, 1),
                new ItemDropChance(0, 2)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.BeeHat, 1),
                new ItemDropChance(ItemID.BeeShirt, 1),
                new ItemDropChance(ItemID.BeePants, 1),
                new ItemDropChance(0, 6)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.QueenBeeTrophy, 1),
                new ItemDropChance(0, 9)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.Beenade, 3, 10, 29),
                new ItemDropChance(0, 1)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.BottledHoney, 1, 5, 15)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.BeeMask, 1),
                new ItemDropChance(0, 6)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.BeeWax, 1, 16, 26)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.GoldCoin, 1, 10)
            })
        });
        public static readonly LootTable QueenBeeExpert = new LootTable(new List<ItemDropSet>{
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.BeeGun, 1),
                new ItemDropChance(ItemID.BeeKeeper, 1),
                new ItemDropChance(ItemID.BeesKnees, 1)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.HoneyComb, 1),
                new ItemDropChance(0, 2)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.Nectar, 1),
                new ItemDropChance(0, 8)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.HoneyedGoggles, 1),
                new ItemDropChance(0, 8)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.HiveWand, 1)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.BeeHat, 1),
                new ItemDropChance(ItemID.BeeShirt, 1),
                new ItemDropChance(ItemID.BeePants, 1)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.QueenBeeTrophy, 1),
                new ItemDropChance(0, 9)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.Beenade, 1, 10, 29)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.BottledHoney, 1, 5, 15)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.BeeMask, 1),
                new ItemDropChance(0, 6)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.BeeWax, 1, 17, 29)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.GoldCoin, 1, 10)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.HiveBackpack, 1)
            })
        });
        #endregion

        // Hard Mode
        #region HardMode
        public static readonly LootTable WallOfFleshNormal = new LootTable(new List<ItemDropSet>(){
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.FleshMask, 1),
                new ItemDropChance(0, 6)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.WallofFleshTrophy, 1),
                new ItemDropChance(0, 9)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.WarriorEmblem, 3),
                new ItemDropChance(ItemID.SorcererEmblem, 3),
                new ItemDropChance(ItemID.RangerEmblem, 3),
                new ItemDropChance(ItemID.SummonerEmblem, 3),
                new ItemDropChance(ItemID.ClockworkAssaultRifle, 4),
                new ItemDropChance(ItemID.LaserRifle, 4),
                new ItemDropChance(ItemID.BreakerBlade, 4)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.HealingPotion, 1, 5, 15)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.Pwnhammer, 1)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.GoldCoin, 1, 8)
            })
        });
        public static readonly LootTable WallOfFleshExpert = new LootTable(new List<ItemDropSet>(){
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.FleshMask, 1),
                new ItemDropChance(0, 6)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.WallofFleshTrophy, 1),
                new ItemDropChance(0, 9)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.WarriorEmblem, 1),
                new ItemDropChance(ItemID.SorcererEmblem, 1),
                new ItemDropChance(ItemID.RangerEmblem, 1),
                new ItemDropChance(ItemID.SummonerEmblem, 1)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.ClockworkAssaultRifle, 1),
                new ItemDropChance(ItemID.LaserRifle, 1),
                new ItemDropChance(ItemID.BreakerBlade, 1)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.HealingPotion, 1, 5, 15)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.Pwnhammer, 1)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.GoldCoin, 1, 8)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.DemonHeart, 1)
            })
        });
        
        public static readonly LootTable TheTwinsNormal = new LootTable(new List<ItemDropSet>()
        {
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.TwinMask, 1),
                new ItemDropChance(ItemID.None, 6)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.SoulofSight, 1, 20, 40)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.HallowedBar, 1, 15, 30)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.GoldCoin, 1, 12)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.GreaterHealingPotion, 1, 5, 15)
            })
        });
        public static readonly LootTable TheTwinsExpert = new LootTable(new List<ItemDropSet>()
        {
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.TwinMask, 1),
                new ItemDropChance(ItemID.None, 6)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.SoulofSight, 1, 25, 40)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.HallowedBar, 1, 20, 35)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.GoldCoin, 1, 12)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.GreaterHealingPotion, 1, 5, 15)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.MechanicalWheelPiece, 1)
            })
        });

        public static readonly LootTable SkeletronPrimeNormal = new LootTable(new List<ItemDropSet>()
        {
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.SkeletronPrimeMask, 1),
                new ItemDropChance(ItemID.None, 6)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.SkeletronPrimeTrophy, 1),
                new ItemDropChance(ItemID.None, 9)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.SoulofFright, 1, 20, 40)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.HallowedBar, 1, 15, 30)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.GoldCoin, 1, 12)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.GreaterHealingPotion, 1, 5, 15)
            })
        });
        public static readonly LootTable SkeletronPrimeExpert = new LootTable(new List<ItemDropSet>()
        {
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.SkeletronPrimeMask, 1),
                new ItemDropChance(ItemID.None, 6)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.SkeletronPrimeTrophy, 1),
                new ItemDropChance(ItemID.None, 9)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.SoulofFright, 1, 25, 40)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.HallowedBar, 1, 20, 35)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.GoldCoin, 1, 12)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.GreaterHealingPotion, 1, 5, 15)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.MechanicalBatteryPiece, 1)
            })
        });

        public static readonly LootTable TheDestroyerNormal = new LootTable(new List<ItemDropSet>()
        {
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.DestroyerMask, 1),
                new ItemDropChance(ItemID.None, 6)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.DestroyerTrophy, 1),
                new ItemDropChance(ItemID.None, 9)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.SoulofMight, 1, 20, 40)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.HallowedBar, 1, 15, 30)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.GoldCoin, 1, 12)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.GreaterHealingPotion, 1, 5, 15)
            })
        });
        public static readonly LootTable TheDestroyerExpert = new LootTable(new List<ItemDropSet>()
        {
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.DestroyerMask, 1),
                new ItemDropChance(ItemID.None, 6)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.DestroyerTrophy, 1),
                new ItemDropChance(ItemID.None, 9)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.SoulofMight, 1, 25, 40)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.HallowedBar, 1, 20, 35)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.GoldCoin, 1, 12)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.GreaterHealingPotion, 1, 5, 15)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.MechanicalWagonPiece, 1)
            })
        });

        public static readonly LootTable PlanteraNormal = new LootTable(new List<ItemDropSet>()
        {
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.PlanteraMask, 1),
                new ItemDropChance(ItemID.None, 6)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.PlanteraTrophy, 1),
                new ItemDropChance(ItemID.None, 9)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.TempleKey, 1)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.Seedling, 1),
                new ItemDropChance(ItemID.None, 19)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.PygmyStaff, 1),
                new ItemDropChance(ItemID.None, 3)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.ThornHook, 1),
                new ItemDropChance(ItemID.None, 9)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.TheAxe, 1),
                new ItemDropChance(ItemID.None, 49)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.GrenadeLauncher, 1, new ItemDropChance(ItemID.RocketI, 1, 20, 49)),
                new ItemDropChance(ItemID.VenusMagnum, 1),
                new ItemDropChance(ItemID.NettleBurst, 1),
                new ItemDropChance(ItemID.LeafBlower, 1),
                new ItemDropChance(ItemID.FlowerPow, 1),
                new ItemDropChance(ItemID.WaspGun, 1),
                new ItemDropChance(ItemID.Seedler, 1)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.GoldCoin, 1, 15)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.GreaterHealingPotion, 1, 5, 15)
            })
        });
        public static readonly LootTable PlanteraNormalFirstKill = new LootTable(new List<ItemDropSet>()
        {
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.PlanteraMask, 1),
                new ItemDropChance(ItemID.None, 6)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.PlanteraTrophy, 1),
                new ItemDropChance(ItemID.None, 9)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.TempleKey, 1)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.Seedling, 1),
                new ItemDropChance(ItemID.None, 19)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.PygmyStaff, 1),
                new ItemDropChance(ItemID.None, 3)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.ThornHook, 1),
                new ItemDropChance(ItemID.None, 9)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.TheAxe, 1),
                new ItemDropChance(ItemID.None, 49)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.GrenadeLauncher, 1, new ItemDropChance(ItemID.RocketI, 1, 20, 49))
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.GoldCoin, 1, 15)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.GreaterHealingPotion, 1, 5, 15)
            })
        });
        public static readonly LootTable PlanteraExpert = new LootTable(new List<ItemDropSet>()
        {
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.PlanteraMask, 1),
                new ItemDropChance(ItemID.None, 6)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.PlanteraTrophy, 1),
                new ItemDropChance(ItemID.None, 9)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.TempleKey, 1)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.Seedling, 1),
                new ItemDropChance(ItemID.None, 14)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.PygmyStaff, 1),
                new ItemDropChance(ItemID.None, 1)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.ThornHook, 1),
                new ItemDropChance(ItemID.None, 9)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.TheAxe, 1),
                new ItemDropChance(ItemID.None, 19)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.GrenadeLauncher, 1, new ItemDropChance(ItemID.RocketI, 1, 15, 149)),
                new ItemDropChance(ItemID.VenusMagnum, 1),
                new ItemDropChance(ItemID.NettleBurst, 1),
                new ItemDropChance(ItemID.LeafBlower, 1),
                new ItemDropChance(ItemID.FlowerPow, 1),
                new ItemDropChance(ItemID.WaspGun, 1),
                new ItemDropChance(ItemID.Seedler, 1)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.GoldCoin, 1, 15)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.GreaterHealingPotion, 1, 5, 15)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.SporeSac, 1)
            })
        });

        public static readonly LootTable GolemNormal = new LootTable(new List<ItemDropSet>()
        {
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.GolemMask, 1),
                new ItemDropChance(ItemID.None, 6)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.GolemTrophy, 1),
                new ItemDropChance(ItemID.None, 9)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.BeetleHusk, 1, 4, 8)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.Stynger, 1, new ItemDropChance(ItemID.StyngerBolt, 1, 60, 99)),
                new ItemDropChance(ItemID.PossessedHatchet, 1),
                new ItemDropChance(ItemID.SunStone, 1),
                new ItemDropChance(ItemID.EyeoftheGolem, 1),
                new ItemDropChance(ItemID.Picksaw, 1),
                new ItemDropChance(ItemID.HeatRay, 1),
                new ItemDropChance(ItemID.StaffofEarth, 1),
                new ItemDropChance(ItemID.GolemFist, 1)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.GoldCoin, 1, 15)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.GreaterHealingPotion, 1, 5, 15)
            })
        });
        public static readonly LootTable GolemExpert = new LootTable(new List<ItemDropSet>()
        {
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.GolemMask, 1),
                new ItemDropChance(ItemID.None, 6)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.GolemTrophy, 1),
                new ItemDropChance(ItemID.None, 9)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.BeetleHusk, 1, 4, 8)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.Stynger, 1, new ItemDropChance(ItemID.StyngerBolt, 1, 60, 99)),
                new ItemDropChance(ItemID.PossessedHatchet, 1),
                new ItemDropChance(ItemID.SunStone, 1),
                new ItemDropChance(ItemID.EyeoftheGolem, 1),
                new ItemDropChance(ItemID.Picksaw, 1),
                new ItemDropChance(ItemID.HeatRay, 1),
                new ItemDropChance(ItemID.StaffofEarth, 1),
                new ItemDropChance(ItemID.GolemFist, 1)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.GoldCoin, 1, 15)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.GreaterHealingPotion, 1, 5, 15)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.ShinyStone, 1)
            })
        });

        public static readonly LootTable DukeFishronNormal = new LootTable(new List<ItemDropSet>()
        {
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.DukeFishronMask, 1),
                new ItemDropChance(ItemID.None, 6)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.DukeFishronTrophy, 1),
                new ItemDropChance(ItemID.None, 9)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.FishronWings, 1),
                new ItemDropChance(ItemID.None, 14)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.Flairon, 1),
                new ItemDropChance(ItemID.Tsunami, 1),
                new ItemDropChance(ItemID.RazorbladeTyphoon, 1),
                new ItemDropChance(ItemID.TempestStaff, 1),
                new ItemDropChance(ItemID.BubbleGun, 1)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.GoldCoin, 1, 10)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.GreaterHealingPotion, 1, 5, 15)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.ShrimpyTruffle, 1)
            })
        });
        public static readonly LootTable DukeFishronExpert = new LootTable(new List<ItemDropSet>()
        {
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.DukeFishronMask, 1),
                new ItemDropChance(ItemID.None, 6)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.DukeFishronTrophy, 1),
                new ItemDropChance(ItemID.None, 9)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.FishronWings, 1),
                new ItemDropChance(ItemID.None, 9)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.Flairon, 1),
                new ItemDropChance(ItemID.Tsunami, 1),
                new ItemDropChance(ItemID.RazorbladeTyphoon, 1),
                new ItemDropChance(ItemID.TempestStaff, 1),
                new ItemDropChance(ItemID.BubbleGun, 1)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.GoldCoin, 1, 10)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.GreaterHealingPotion, 1, 5, 15)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.ShrimpyTruffle, 1)
            })
        });

        public static readonly LootTable MoonLordNormal = new LootTable(new List<ItemDropSet>()
        {
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.MoonMask, 1),
                new ItemDropChance(ItemID.None, 6)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.MoonLordTrophy, 1),
                new ItemDropChance(ItemID.None, 9)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.PortalGun, 1)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.LunarOre, 1, 70, 90)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.Meowmere, 1),
                new ItemDropChance(ItemID.Terrarian, 1),
                new ItemDropChance(ItemID.StarWrath, 1),
                new ItemDropChance(ItemID.SDMG, 1),
                new ItemDropChance(ItemID.FireworksLauncher, 1),
                new ItemDropChance(ItemID.LastPrism, 1),
                new ItemDropChance(ItemID.LunarFlareBook, 1),
                new ItemDropChance(ItemID.RainbowCrystalStaff, 1),
                new ItemDropChance(ItemID.MoonlordTurretStaff, 1)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.GreaterHealingPotion, 1, 5, 15)
            })
        });
        public static readonly LootTable MoonLordExpert = new LootTable(new List<ItemDropSet>()
        {
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.BossMaskMoonlord, 1),
                new ItemDropChance(ItemID.None, 6)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.MoonLordTrophy, 1),
                new ItemDropChance(ItemID.None, 9)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.PortalGun, 1)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.LunarOre, 1, 90, 110)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.Meowmere, 1),
                new ItemDropChance(ItemID.Terrarian, 1),
                new ItemDropChance(ItemID.StarWrath, 1),
                new ItemDropChance(ItemID.SDMG, 1),
                new ItemDropChance(ItemID.FireworksLauncher, 1),
                new ItemDropChance(ItemID.LastPrism, 1),
                new ItemDropChance(ItemID.LunarFlareBook, 1),
                new ItemDropChance(ItemID.RainbowCrystalStaff, 1),
                new ItemDropChance(ItemID.MoonlordTurretStaff, 1)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.GreaterHealingPotion, 1, 5, 15)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.GravityGlobe, 1)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.SuspiciousLookingTentacle, 1)
            })
        });
        #endregion HardMode

        // Bosses without treasure bags in vanilla
        #region MiscBosses
        public static readonly LootTable MartianSaucerNormal = new LootTable(new List<ItemDropSet>()
        {
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.Xenopopper),
                new ItemDropChance(ItemID.XenoStaff),
                new ItemDropChance(ItemID.LaserMachinegun),
                new ItemDropChance(ItemID.LaserDrill),
                new ItemDropChance(ItemID.ElectrosphereLauncher),
                new ItemDropChance(ItemID.ChargedBlasterCannon),
                new ItemDropChance(ItemID.InfluxWaver),
                new ItemDropChance(ItemID.CosmicCarKey),
                new ItemDropChance(ItemID.AntiGravityHook)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.MartianSaucerTrophy, 1),
                new ItemDropChance(ItemID.None, 9)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.GreaterHealingPotion, 1, 5, 15)
            })
        });
        public static readonly LootTable MartianSaucerExpert = new LootTable(new List<ItemDropSet>()
        {
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.Xenopopper),
                new ItemDropChance(ItemID.XenoStaff),
                new ItemDropChance(ItemID.LaserMachinegun),
                new ItemDropChance(ItemID.LaserDrill),
                new ItemDropChance(ItemID.ElectrosphereLauncher),
                new ItemDropChance(ItemID.ChargedBlasterCannon),
                new ItemDropChance(ItemID.InfluxWaver),
                new ItemDropChance(ItemID.CosmicCarKey),
                new ItemDropChance(ItemID.AntiGravityHook)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.MartianSaucerTrophy, 1),
                new ItemDropChance(ItemID.None, 9)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.GreaterHealingPotion, 1, 5, 15)
            })
        });

        public static readonly LootTable FlyingDutchmanNormal = new LootTable(new List<ItemDropSet>()
        {
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.CoinGun, 1),
                new ItemDropChance(ItemID.None, 399)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.LuckyCoin, 1),
                new ItemDropChance(ItemID.None, 199)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.DiscountCard, 1),
                new ItemDropChance(ItemID.None, 99)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.PirateStaff, 1),
                new ItemDropChance(ItemID.None, 99)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.GoldRing, 1),
                new ItemDropChance(ItemID.None, 49)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.Cutlass, 1),
                new ItemDropChance(ItemID.None, 9)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.FlyingDutchmanTrophy, 1),
                new ItemDropChance(ItemID.None, 9)
            })
        });
        public static readonly LootTable FlyingDutchmanExpert = new LootTable(new List<ItemDropSet>()
        {
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.CoinGun, 1),
                new ItemDropChance(ItemID.None, 399)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.LuckyCoin, 1),
                new ItemDropChance(ItemID.None, 199)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.DiscountCard, 1),
                new ItemDropChance(ItemID.None, 99)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.PirateStaff, 1),
                new ItemDropChance(ItemID.None, 99)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.GoldRing, 1),
                new ItemDropChance(ItemID.None, 49)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.Cutlass, 1),
                new ItemDropChance(ItemID.None, 9)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>(){
                new ItemDropChance(ItemID.FlyingDutchmanTrophy, 1),
                new ItemDropChance(ItemID.None, 9)
            })
        });
        #endregion MiscBosses

        // Developer Items
        #region DevSets

        public static readonly LootTable Red = new LootTable(new List<ItemDropSet>{
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.RedsHelmet, 1)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.RedsBreastplate, 1)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.RedsLeggings, 1)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.RedsWings, 1)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.RedsYoyo, 1)
            })            
        });
        public static readonly LootTable Cenx = new LootTable(new List<ItemDropSet>{
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.CenxsTiara, 1)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.CenxsBreastplate, 1)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.CenxsLeggings, 1)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.CenxsWings, 1)
            })
        });
        public static readonly LootTable Cenx2 = new LootTable(new List<ItemDropSet>{
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.CenxsDress, 1)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.CenxsDressPants, 1)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.CenxsWings, 1)
            })
        });
        public static readonly LootTable Crowno = new LootTable(new List<ItemDropSet>{
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.CrownosMask, 1)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.CrownosBreastplate, 1)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.CrownosLeggings, 1)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.CrownosWings, 1)
            })
        });
        public static readonly LootTable Will = new LootTable(new List<ItemDropSet>{
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.WillsHelmet, 1)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.WillsBreastplate, 1)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.WillsLeggings, 1)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.WillsWings, 1)
            })
        });
        public static readonly LootTable Jim = new LootTable(new List<ItemDropSet>{
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.JimsHelmet, 1)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.JimsBreastplate, 1)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.JimsLeggings, 1)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.JimsWings, 1)
            })
        });
        public static readonly LootTable Aaron = new LootTable(new List<ItemDropSet>{
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.AaronsHelmet, 1)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.AaronsBreastplate, 1)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.AaronsLeggings, 1)
            })
        });
        public static readonly LootTable DTown = new LootTable(new List<ItemDropSet>{
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.DTownsHelmet, 1)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.DTownsBreastplate, 1)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.DTownsLeggings, 1)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.DTownsWings, 1)
            })
        });
        public static readonly LootTable Lazure = new LootTable(new List<ItemDropSet>{
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.BejeweledValkyrieHead, 1)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.BejeweledValkyrieBody, 1)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.BejeweledValkyrieWing, 1)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.ValkyrieYoyo, 1)
            })
        });
        public static readonly LootTable Yoraiz0r = new LootTable(new List<ItemDropSet>{
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.Yoraiz0rHead, 1)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.Yoraiz0rPants, 1)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.Yoraiz0rShirt, 1)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.Yoraiz0rDarkness, 1)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.Yoraiz0rWings, 1)
            })
        });
        public static readonly LootTable Skiph = new LootTable(new List<ItemDropSet>{
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.SkiphsHelm, 1)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.SkiphsShirt, 1)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.SkiphsPants, 1)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.SkiphsWings, 1)
            })
        });
        public static readonly LootTable Loki = new LootTable(new List<ItemDropSet>{
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.LokisHelm, 1)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.LokisShirt, 1)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.LokisPants, 1)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.LokisWings, 1)
            }),
            new ItemDropSet(new HashSet<ItemDropChance>{
                new ItemDropChance(ItemID.LokisDye, 1)
            })
        });
        public static readonly List<LootTable> DevSets = new List<LootTable>(){
            Red,
            Cenx,
            Cenx2,
            Crowno,
            Will,
            Jim,
            Aaron,
            DTown,
            Lazure,
            Yoraiz0r,
            Skiph,
            Loki
        };

        #endregion DevSets
    }
}
