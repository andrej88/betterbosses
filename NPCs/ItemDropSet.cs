﻿using System;
using System.Collections.Generic;
using Terraria;
using Terraria.ModLoader;

namespace BetterBosses.NPCs
{
    /// <summary>
    /// A set of items, where 'trials' number of items will drop.
    /// </summary>
    class ItemDropSet
    {
        private HashSet<ItemDropChance> items;
        private int trials;

        public ItemDropSet(ICollection<ItemDropChance> items, int trials = 1)
        {
            if (trials > items.Count)
                throw new ArgumentException("Number of trials exceeds number of items in set", "trials");
            else if (items.Count > 1 && trials == items.Count)
                ErrorLogger.Log("Every item in the set will drop. Use multiple sets of one instead");
            else if (trials < 1)
                throw new ArgumentException("There must be at least one trial", "trials");

            this.items = new HashSet<ItemDropChance>(items);
            this.trials = trials;
        }

        public ItemDropSet()
        {
            this.items = new HashSet<ItemDropChance>();
            this.trials = 1;
        }

        /// <summary>
        /// Gets random items from the set of possible drops. Number of items equals 'trials' member variable.
        /// </summary>
        /// <returns>A lits of ItemStacks to be dropped.</returns>
        public List<ItemStack> ItemsToDrop()
        {
            List<ItemStack> drops = new List<ItemStack>();
            
            HashSet<ItemDropChance> possibleDrops = new HashSet<ItemDropChance>();
            foreach (ItemDropChance i in items)
            {
                possibleDrops.Add(i);
            }

            for (int i = 0; i < trials; i++)
            {
                ItemDropChance itemToDrop = GetRandomItem(possibleDrops);
                if (itemToDrop.GetID() > 0) {
                    AddItemToDrops(drops, itemToDrop);
                }
                possibleDrops.Remove(itemToDrop);
            }

            return drops;
        }

        private static void AddItemToDrops(List<ItemStack> drops, ItemDropChance itemToDrop)
        {
            drops.Add(new ItemStack(itemToDrop.GetID(), Main.rand.Next(itemToDrop.GetMin(), itemToDrop.GetMax() + 1)));
            ItemDropChance companion = itemToDrop.GetCompanion();
            if (companion != null)
            {
                AddItemToDrops(drops, companion);
            }
        }

        /// <summary>
        /// Gets a random Item ID taking into account the weights of items in the given set
        /// </summary>
        /// <param name="set">Set to choose an item from</param>
        /// <returns></returns>
        private ItemDropChance GetRandomItem(HashSet<ItemDropChance> set)
        {
            int totalWeight = 0;
            
            foreach (ItemDropChance c in set)
            {
                totalWeight += c.GetWeight();
            }

            int randomNumber = Main.rand.Next(totalWeight);

            foreach (ItemDropChance c in set)
            {
                if (randomNumber < c.GetWeight())
                {
                    return c;
                }

                randomNumber = randomNumber - c.GetWeight();
            }
            return null;
        }

    }
}
