﻿using System;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace BetterBosses.Items
{
    public class SkeletronSpawnItem : ModItem
    {
        public override void SetDefaults()
        {
            item.name = "Skeletron";
            item.width = 20;
            item.height = 20;
            item.maxStack = 20;
            item.value = 100;
            item.rare = 1;
            item.useAnimation = 30;
            item.useTime = 30;
            item.useStyle = 4;
            item.consumable = true;
        }

        public override bool UseItem(Player player)
        {
            Main.time = 0f;
            NPC.SpawnOnPlayer(player.whoAmI, NPCID.SkeletronHead);
            Main.PlaySound(15, (int)player.position.X, (int)player.position.Y, 0);
            return true;
        }

        public override void AddRecipes()
        {
            ModRecipe recipe = new ModRecipe(mod);
            recipe.AddIngredient(ItemID.DirtBlock, 1);
            recipe.SetResult(this, 20);
            recipe.AddRecipe();
        }
    }
}