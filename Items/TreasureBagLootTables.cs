﻿using BetterBosses.NPCs;
using System.Collections.Generic;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace BetterBosses.Items
{
    class TreasureBagLootTables : GlobalItem
    {

        public override bool PreOpenVanillaBag(string context, Terraria.Player player, int arg)
        {
            if (context == "bossBag")
            {
                // Drop dev sets
                if (arg == ItemID.SkeletronPrimeBossBag ||
                    arg == ItemID.TwinsBossBag ||
                    arg == ItemID.DestroyerBossBag ||
                    arg == ItemID.PlanteraBossBag ||
                    arg == ItemID.GolemBossBag ||
                    arg == ItemID.FishronBossBag ||
                    arg == ItemID.MoonLordBossBag)
                {
                    if (Main.rand.Next(20) == 0)
                    {
                        ErrorLogger.Log("Dropping Dev Set");
                        List<ItemStack> drops = Utils.SelectRandom<LootTable>(Main.rand, VanillaLootTables.DevSets.ToArray()).GetRandomLootDrop();
                        foreach (ItemStack i in drops)
                        {
                            player.QuickSpawnItem(i.id, i.stack);
                        }
                    }
                }

                if (arg == ItemID.KingSlimeBossBag)
                {
                    List<ItemStack> drops = VanillaLootTables.KingSlimeExpert.GetRandomLootDrop();
                    foreach (ItemStack i in drops)
                    {
                        player.QuickSpawnItem(i.id, i.stack);
                    }
                    return false;
                }

                if (arg == ItemID.EyeOfCthulhuBossBag)
                {
                    if (WorldGen.crimson)
                    {
                        List<ItemStack> drops = VanillaLootTables.EyeOfCthulhuExpertCrimson.GetRandomLootDrop();
                        foreach (ItemStack i in drops)
                        {
                            player.QuickSpawnItem(i.id, i.stack);
                        }
                    }
                    else
                    {
                        {
                            List<ItemStack> drops = VanillaLootTables.EyeOfCthulhuExpertCorrupt.GetRandomLootDrop();
                            foreach (ItemStack i in drops)
                            {
                                player.QuickSpawnItem(i.id, i.stack);
                            }
                        }
                    }

                    return false;
                }

                if (arg == ItemID.BrainOfCthulhuBossBag)
                {
                    List<ItemStack> drops = VanillaLootTables.BrainOfCthulhuExpert.GetRandomLootDrop();
                    foreach (ItemStack i in drops)
                    {
                        player.QuickSpawnItem(i.id, i.stack);
                    }
                    return false;
                }

                if (arg == ItemID.EaterOfWorldsBossBag)
                {
                    List<ItemStack> drops = VanillaLootTables.EaterOfWorldsExpert.GetRandomLootDrop();
                    foreach (ItemStack i in drops)
                    {
                        player.QuickSpawnItem(i.id, i.stack);
                    }
                    return false;
                }

                if (arg == ItemID.SkeletronBossBag)
                {
                    List<ItemStack> drops = VanillaLootTables.SkeletronExpert.GetRandomLootDrop();
                    foreach (ItemStack i in drops)
                    {
                        player.QuickSpawnItem(i.id, i.stack);
                    }
                    return false;
                }

                if (arg == ItemID.QueenBeeBossBag)
                {
                    List<ItemStack> drops = VanillaLootTables.QueenBeeExpert.GetRandomLootDrop();
                    foreach (ItemStack i in drops)
                    {
                        player.QuickSpawnItem(i.id, i.stack);
                    }
                    return false;
                }

                if (arg == ItemID.WallOfFleshBossBag)
                {
                    List<ItemStack> drops = VanillaLootTables.WallOfFleshExpert.GetRandomLootDrop();
                    foreach (ItemStack i in drops)
                    {
                        if (i.id != ItemID.DemonHeart || !player.extraAccessory)
                        {
                            player.QuickSpawnItem(i.id, i.stack);
                        }
                    }
                    return false;
                }

                if (arg == ItemID.TwinsBossBag)
                {
                    List<ItemStack> drops = VanillaLootTables.TheTwinsExpert.GetRandomLootDrop();
                    foreach (ItemStack i in drops)
                    {
                        player.QuickSpawnItem(i.id, i.stack);
                    }
                    return false;
                }

                if (arg == ItemID.DestroyerBossBag)
                {
                    List<ItemStack> drops = VanillaLootTables.TheDestroyerExpert.GetRandomLootDrop();
                    foreach (ItemStack i in drops)
                    {
                        player.QuickSpawnItem(i.id, i.stack);
                    }
                    return false;
                }

                if (arg == ItemID.SkeletronPrimeBossBag)
                {
                    List<ItemStack> drops = VanillaLootTables.SkeletronPrimeExpert.GetRandomLootDrop();
                    foreach (ItemStack i in drops)
                    {
                        player.QuickSpawnItem(i.id, i.stack);
                    }
                    return false;
                }

                if (arg == ItemID.PlanteraBossBag)
                {
                    List<ItemStack> drops = VanillaLootTables.PlanteraExpert.GetRandomLootDrop();
                    foreach (ItemStack i in drops)
                    {
                        player.QuickSpawnItem(i.id, i.stack);
                    }
                    return false;
                }

                if (arg == ItemID.GolemBossBag)
                {
                    List<ItemStack> drops = VanillaLootTables.GolemExpert.GetRandomLootDrop();
                    foreach (ItemStack i in drops)
                    {
                        player.QuickSpawnItem(i.id, i.stack);
                    }
                    return false;
                }

                if (arg == ItemID.FishronBossBag)
                {
                    List<ItemStack> drops = VanillaLootTables.DukeFishronExpert.GetRandomLootDrop();
                    foreach (ItemStack i in drops)
                    {
                        player.QuickSpawnItem(i.id, i.stack);
                    }
                    return false;
                }

                if (arg == ItemID.MoonLordBossBag)
                {
                    List<ItemStack> drops = VanillaLootTables.MoonLordExpert.GetRandomLootDrop();
                    foreach (ItemStack i in drops)
                    {
                        if (i.id != ItemID.PortalGun || !player.HasItem(ItemID.PortalGun))
                        {
                            player.QuickSpawnItem(i.id, i.stack);
                        }
                    }
                    return false;
                }
            }

            return true;
        }

    }
}
