using Terraria.ID;
using Terraria.ModLoader;


namespace BetterBosses {
public class BetterBosses : Mod
{
    public override void SetModInfo(out string name, ref ModProperties properties)
    {
        name = "BetterBosses";
        properties.Autoload = true;
        properties.AutoloadGores = true;
        properties.AutoloadSounds = true;
    }

    public override void Load()
    {
        
    }

    public override void AddCraftGroups()
    {
        
    }

    public override void AddRecipes()
    {
        CheatyDevRecipes();
    }

    private void CheatyDevRecipes()
    {
        int[] bossBags = new int[]{
            ItemID.KingSlimeBossBag,
            ItemID.EyeOfCthulhuBossBag,
            ItemID.EaterOfWorldsBossBag,
            ItemID.BrainOfCthulhuBossBag,
            ItemID.QueenBeeBossBag,
            ItemID.SkeletronBossBag,
            ItemID.WallOfFleshBossBag,
            ItemID.TwinsBossBag,
            ItemID.SkeletronPrimeBossBag,
            ItemID.DestroyerBossBag,
            ItemID.PlanteraBossBag,
            ItemID.GolemBossBag,
            ItemID.FishronBossBag,
            ItemID.MoonLordBossBag
        };

        int[] spawningItems = new int[]{
            ItemID.SlimeCrown,
            ItemID.SuspiciousLookingEye,
            ItemID.WormFood,
            ItemID.BloodySpine,
            ItemID.Abeemination,
            ItemID.ClothierVoodooDoll,
            ItemID.GuideVoodooDoll,
            ItemID.MechanicalEye,
            ItemID.MechanicalWorm,
            ItemID.MechanicalSkull,
            ItemID.LihzahrdPowerCell,
            ItemID.CelestialSigil
        };

        int[] cheatyItems = new int[]{
            ItemID.SDMG,
            ItemID.SolarFlareHelmet,
            ItemID.SolarFlareBreastplate,
            ItemID.SolarFlareLeggings,
            ItemID.WingsVortex,
            ItemID.FrostsparkBoots,
            ItemID.AnkhShield,
            ItemID.CellPhone,
            ItemID.LunarHamaxeVortex,
            ItemID.VortexPickaxe,
            ItemID.DrillContainmentUnit,
            ItemID.StardustDragonStaff,
            ItemID.LihzahrdAltar,
        };

        ModRecipe recipe;

        foreach (int i in spawningItems)
        {
            recipe = new ModRecipe(this);
            recipe.AddIngredient(ItemID.DirtBlock);
            recipe.SetResult(i, 20);
            recipe.AddRecipe();
        }

        foreach (int i in cheatyItems)
        {
            recipe = new ModRecipe(this);
            recipe.AddIngredient(ItemID.DirtBlock);
            recipe.SetResult(i);
            recipe.AddRecipe();
        }

        foreach (int i in bossBags)
        {
            recipe = new ModRecipe(this);
            recipe.AddIngredient(ItemID.DirtBlock);
            recipe.SetResult(i);
            recipe.AddRecipe();
        }

        recipe = new ModRecipe(this);
        recipe.AddIngredient(ItemID.DirtBlock);
        recipe.SetResult(ItemID.DirtBlock, 999);
        recipe.AddRecipe();

        recipe = new ModRecipe(this);
        recipe.AddIngredient(ItemID.DirtBlock);
        recipe.SetResult(ItemID.ChlorophyteBullet, 999);
        recipe.AddRecipe();
        
        recipe = new ModRecipe(this);
        recipe.AddIngredient(ItemID.DirtBlock);
        recipe.SetResult(ItemID.CrystalBullet, 999);
        recipe.AddRecipe();
        
        recipe = new ModRecipe(this);
        recipe.AddIngredient(ItemID.DirtBlock);
        recipe.SetResult(ItemID.VenomBullet, 999);
        recipe.AddRecipe();

        recipe = new ModRecipe(this);
        recipe.AddIngredient(ItemID.DirtBlock);
        recipe.SetResult(ItemID.HighVelocityBullet, 999);
        recipe.AddRecipe();

        recipe = new ModRecipe(this);
        recipe.AddIngredient(ItemID.DirtBlock);
        recipe.SetResult(ItemID.SuperHealingPotion, 30);
        recipe.AddRecipe();
    }
    
}}